import fs from 'fs';
import { CustomerNeed, loadNeedWithDetails } from './customerNeed';
import { FeatureRecord } from './featureRecord';

interface FeatureWithNeed {
    feature: string;
    needs: Array<CustomerNeed>;
    totalOppSize: number;
}

const getCustomersText = (needs: Array<CustomerNeed>) => {
    return needs.filter((need:CustomerNeed)=>!!need).map((need: CustomerNeed)=>need.Customer["Company Name"]).join(', ');
} 

const getNeedText = (need: CustomerNeed)=>{
    if(!need) return "";
    const description = need["Description of Customer Need"] || "";
    return `\n### ${need.Customer["Company Name"]}: ${need.Summary}\n\n${description.replace('--', '')}\n`;
};

const getFeatureText = (feature: FeatureWithNeed) => {
    let featureText = `\n## ${feature.feature}\n\nTotal opportunity: ${Math.round(feature.totalOppSize /1000)} kUSD \nCustomers: ${getCustomersText(feature.needs)}\n`;
    feature.needs.forEach(feature=>{
        featureText += getNeedText(feature);
    });
    return featureText;
}

const getFeaturesAndNeedsText = (features: FeatureWithNeed[]) => {
    let text = "";
    features.forEach(feature=>{
        text += getFeatureText(feature);
    });
    return text;
}

const filterFeatureNeedsOnLostOpps = (needs: CustomerNeed[])=>{
    return needs.filter((need)=>{
        return need && need["Opportunity ARR Rollup"] > 0;
    })
}

const filterNeedsOnLostOpps = (features: FeatureWithNeed[]) => {
    features.forEach((feature)=>{
        feature.needs = filterFeatureNeedsOnLostOpps(feature.needs);
    });
}

const calculateFeatureOppValue = (needs: CustomerNeed[]) => {
    let value = 0;
    needs.forEach((need)=>{value += need["Opportunity ARR Rollup"]});
    return value;
}

const calculateFeaturesOppValue = (features: FeatureWithNeed[]) => {
    features.forEach((feature)=>{
        feature.totalOppSize = calculateFeatureOppValue(feature.needs);
    });
}

const removeDuplicateCustomerNeedsFromFeature = (needs: CustomerNeed[]) => {
    const existingCustomers: string[] = [];
    return needs.filter((need)=>{
        if(existingCustomers.includes(need.Customer["Company Name"]))
            return false;
        existingCustomers.push(need.Customer["Company Name"]);
        return true;
    });
};

const removeDuplicateCustomerNeeds = (features: FeatureWithNeed[]) => {
    features.forEach((feature)=>{
        feature.needs = removeDuplicateCustomerNeedsFromFeature(feature.needs);
    })
}

const loadFeatureWithNeeds = (feature: FeatureRecord): Promise<FeatureWithNeed> => {
    return new Promise((resolve, reject) => { 
        const featureWithNeed: FeatureWithNeed = {feature: feature.name, needs: [], totalOppSize: 0};
        console.log(`Getting needs for feature "${feature.name}""`);
        Promise.allSettled(
            feature.needs.map((needId)=>loadNeedWithDetails(needId))
        ).then((result)=>{
            const allNeeds = result.map((item: PromiseSettledResult<CustomerNeed>)=>{
                if(item.status === "fulfilled") {
                    return item.value;
                }
                return null}).filter((need)=>need !== null) as CustomerNeed[];
            featureWithNeed.needs = allNeeds;
            console.log(`Loaded feature ${feature.name}`);
            resolve(featureWithNeed);
        }).catch((err)=>{
            reject(err);
        });
    });
}

const loadAllFeaturesWithNeeds = (features: FeatureRecord[]): Promise<FeatureWithNeed>[] => {
    return features.map((feature) => loadFeatureWithNeeds(feature));
}

const features: Array<FeatureRecord> = JSON.parse(Buffer.from(fs.readFileSync('prioritized.json')).toString())

let featuresWithNeeds: Array<FeatureWithNeed> = [];


Promise.allSettled(loadAllFeaturesWithNeeds(features)).then((result)=>{
    featuresWithNeeds = result.map((item)=>{
        if(item.status === "fulfilled") {
            return item.value;
        }
        return null
    }).filter((item)=>item !== null) as Array<FeatureWithNeed>;
    filterNeedsOnLostOpps(featuresWithNeeds);
    removeDuplicateCustomerNeeds(featuresWithNeeds);
    calculateFeaturesOppValue(featuresWithNeeds);
    featuresWithNeeds.sort((a, b)=> {
         if (a.totalOppSize < b.totalOppSize) return 1;
         return -1;
        });
    fs.writeFileSync('features-with-needs.json', JSON.stringify(featuresWithNeeds));
    const markdown = getFeaturesAndNeedsText(featuresWithNeeds);
    fs.writeFileSync('features-with-needs.md', markdown);
    console.log(markdown);
}).catch((err)=>{
    console.error(err);
});