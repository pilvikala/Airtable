# Airtable feature prioritization by needs and opportunity size

The script finds top X features based on the number of customer needs and their opportunity size and generates a markdown (and JSON) document with an overview of the needs.

## Getting started

Install typescript and ts-node: `npm install -g ts-node typescript '@types/node'`.

Install dependencies: `npm install`.

Copy or rename the `.env.example` to `.env` and edit it to set your Airtable API key.

Edit `settings.ts` to specify features (their names) that should be analyzed.

Run `npm start`.

The results will be written to `features-with-needs.md` and `features-with-needs.json` files.

