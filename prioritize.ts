import fs from 'fs';
import { FeatureRecord, loadFeatures } from './featureRecord';
import './arrayExtensions';
import { featureNames, keepRecords } from './settings';

const compareByNeedsCount = (a: FeatureRecord, b: FeatureRecord) => {
    if (!a.needs) return 1;
    if (!b.needs) return -1;
    if (a.needs.length > b.needs.length) return -1;
    return 1;
}

loadFeatures(featureNames.deduplicate()).then((featureRecords)=>{
        featureRecords.sort(compareByNeedsCount);
        featureRecords.forEach((rec) => {
            console.log(`${rec.name},${rec.needs?.length}`);
        });

        fs.writeFileSync('prioritized.json', JSON.stringify(featureRecords.filter((_, index) => index < keepRecords)));
    }
);