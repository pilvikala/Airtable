import { base } from './airtable';
import { Customer, findCustomer } from "./customer";

export interface CustomerNeedBase {
    Summary: string;
    "Company Name": string;
    "Description of Customer Need": string;
    "Opportunity ARR Rollup": number;
}

export interface CustomerNeed extends CustomerNeedBase {
    Customer: Customer;
}

export interface CustomerNeedRecord extends CustomerNeedBase{
    Customer: string;
}

export const loadNeedWithDetails = async (needId: string): Promise<CustomerNeed> => {
    const need: CustomerNeedRecord = await new Promise((resolve, reject)=>{
        base('💬 Customer Needs').find(needId, (err, record) => {
            if (err) {
                reject(err);
                return
            }
            resolve(record?.fields as any);
        });
    });
    const customer = await findCustomer(need.Customer);
    return { ...need, Customer: customer};
};