import { Attachment, Collaborator } from "airtable";
import { base } from "./airtable";

export interface FeatureRecord {
    name: string;
    needs: readonly string[];
}

export const loadFeatures = async (featureNames: string[]): Promise<FeatureRecord[]> => {

    const result: FeatureRecord[] = await new Promise((resolve, reject) => {
        console.log('loading features');
        const featureRecords: FeatureRecord[] = [];
        let count = 0;
        base('🧰 Features').select({}).eachPage((records, processNextPage) => {
            records.forEach(function(record) {
                count++;
                const column = record.get('Feature');
                const feature = column ? column.toString() : "";
                if(featureNames.includes(feature)) {
                    const relatedNeedsCol = record.get('Related Customer Needs');
                    if(Array.isArray(relatedNeedsCol)) {
                        featureRecords.push({name: feature, needs: relatedNeedsCol || []});
                    } else {
                        featureRecords.push({name: feature, needs: []});
                    }
                }
            });
            console.log(count);
            if(featureRecords.length < featureNames.length) {
                processNextPage();
            } else {
                resolve(featureRecords);
            }
        }, (err)=>{
            if(err) { reject(err); return; }
            resolve(featureRecords);
        });
    });
    return result;
}