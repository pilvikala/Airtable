import { base } from './airtable';

export interface Customer {
    "Company Name": string;
}

export const findCustomer = async (customerId: string): Promise<Customer> => {
    return new Promise((resolve, reject)=>{
        base('🏢 Customers').find(customerId, function(err, record) {
            if (err) { reject(err); return; }
            resolve(
                {
                    "Company Name": record?.fields["Company Name"] ? record.fields["Company Name"].toString() : ""
                });
        });
    });
};
