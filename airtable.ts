import dotenv from 'dotenv';
import Airtable from 'airtable';

dotenv.config();
const base = new Airtable({apiKey: process.env.API_KEY}).base('appm4IwxZuqMa5Kpp');

const getBase = (id: string) => {
    dotenv.config();
    const base = new Airtable({apiKey: process.env.API_KEY}).base(id);
    return base;
}

export { base, getBase };
