// requires a deps.csv file with the following structure on each line:
// [org name]\t[json array with dependencies]

import { readFile, writeFile } from "fs";

function areEqual(a: string[], b: string[]): boolean {
    if (a === b) return true;
    if (!a || !b) return false;
    if (a.length !== b.length) return false;
    const missing = a.find((element)=>!b.includes(element)) 
    return !missing;
}

function containsPackageSet(deduplicated: string[][], packageSet: string[]): boolean {
    return Boolean(deduplicated.find((d)=>areEqual(d, packageSet)));
}

async function main() {
    const content: string = await new Promise((resolve, reject) => {
        readFile(`${__dirname}/deps.csv`, (err, data) => {
            if(err) {
                reject(err);
                return;
            }
            resolve(data.toString());
        })
    });
    const orgsAndPackagesRaw = content.split('\n').filter(l=>Boolean(l.trim())).map((l)=>{
        const parts = l.split('\t');
        return {
            org: parts[0],
            packages: (JSON.parse(parts[1]) as Array<string>).filter(p=> !["root", "_root", "root-node"].includes(p))
        }
    });
    const grouped: {[key: string]: Array<Array<string>>} = {};
    orgsAndPackagesRaw.forEach((orgEntry) => {
        if(!grouped[orgEntry.org]) {
            grouped[orgEntry.org] = [orgEntry.packages];
            return;
        }
        grouped[orgEntry.org].push(orgEntry.packages);
    });
    Object.keys(grouped).forEach((orgName) => {
        const allPackageSets = grouped[orgName];
        const deduplicated: string[][] = [];
        allPackageSets.forEach((pSet)=>{
            if (!containsPackageSet(deduplicated, pSet)) {
                deduplicated.push(pSet);
            }
        });
        grouped[orgName] = deduplicated;
    });
    const allPackages = Object.keys(grouped).map((orgName)=>grouped[orgName]).flat(2);
    allPackages.sort((a: string, b: string)=> a > b ? 1 : a < b ? -1 : 0)

    const packageCounter: {[packageName: string]: number} = {}
    allPackages.forEach((p)=>{
        if(!packageCounter[p]) {
            packageCounter[p] = 1;
            return
        }
        packageCounter[p] += 1;
    });
    const sorted = Object.keys(packageCounter).map((packageName)=>({packageName, count: packageCounter[packageName]})).sort((a,b)=>b.count - a.count)
    const summary = sorted.map((entry)=>`${entry.packageName}\t${entry.count}`).join('\n');
    await new Promise((_resolve, reject)=> {
        writeFile("deps-summary.txt", summary, (err)=>{
            if(err){
                reject(err);
                return
            }
        })
    })
};



main();
