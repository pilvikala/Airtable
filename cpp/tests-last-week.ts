import { FieldSet, Record } from "airtable";
import { AirtableBase } from "airtable/lib/airtable_base";
import { getBase } from "../airtable";

const testsLastWeek = `
`;

const monitoringProjects = `
`

const freeAccountsMonitoringProjects = `
`

const updateTestsLastWeek = async (base: AirtableBase, entries: string[]) => {
    await Promise.all(entries.map(async (entry) => {
        if (!entry) return;
        const parts = entry.split('\t');
        try {
            const matches = await base('Customers running tests').select({ "filterByFormula": `{Account Name} = "${parts[0]}"` }).all();
            if (matches.length == 0) {
                console.log('adding', entry);
                const customers = await base('Customers').select({"filterByFormula": `{Account Name} = "${parts[0]}"`}).all();
                let customerId = customers.length === 1 ? [customers[0].getId()] : undefined
                if (!customerId) {
                    const isPaid = Boolean(parts[1])
                    customerId = [await createCustomer(base, parts[0], parts[1], isPaid, parts[2])];
                }
                await base('Customers running tests').create(
                    { 
                        "Account Name": parts[0], 
                        "CSM": parts[1], 
                        "Email": parts[2], 
                        "Tests run last week": parseInt(parts[3]), 
                        "Count of dependencies": parseInt(parts[4]),
                        "Customer": customerId
                    });
            }
        } catch (err) {
            console.error(err, parts);
            return
        }
    }));
};

const getCustomers = async (base: AirtableBase) => {
    const customers = (await base("Customers").select({})).all();

    return customers;
}

const createCustomer = async (base: AirtableBase, name: string, contact: string, isPaid: boolean, email?: string) => {
    const csm = contact.includes("@") ? undefined : contact.trim();
    let userEmails = contact.includes("@") ? contact.trim() : undefined;
    if (email) {
        userEmails = email
    }
    const customer = {
        "Account Name": name,
        "CSM": csm,
        "User Emails": userEmails,
        "Free account": isPaid ? "No" : "Yes",
    };
    const result = await base("Customers").create(customer, {typecast: true});
    return result.getId();
}

const updateMonitored = async (base: AirtableBase, entries: string[], isPaid: boolean) => {
    await Promise.all(entries.map(async (entry) => {
        if(!entry) return;
        const parts = entry.split('\t');
        try {
            const matches = await base('Monitored projects').select({ "filterByFormula": `{Account name} = "${parts[0]}"` }).all();
            if (matches.length == 0) {
                console.log('adding', entry);
                const customers = await base('Customers').select({"filterByFormula": `{Account Name} = "${parts[0]}"`}).all();
                let customerId = customers.length === 1 ? [customers[0].getId()] : undefined
                if (!customerId) {
                    customerId = [await createCustomer(base, parts[0], parts[1], isPaid)];
                }
                await base('Monitored projects').create(
                    { 
                        "Account name": parts[0], 
                        "Contact": parts[1], 
                        "Monitored projects": parseInt(parts[2]), 
                        "Customers": customerId
                    });
                return
            }
            if(matches.length > 1) {
                console.warn(`too many accounts in monitored projects matching account name ${parts[0]}, skipping update`);
                return;
            }
            if (matches.length == 1 && matches[0].fields["Monitored projects"] !== parseInt(parts[2])) {
                console.log('updating', entry);
                await base('Monitored projects').update(
                    matches[0].id,
                    { 
                        "Monitored projects": parseInt(parts[2]), 
                    });
            }
        } catch (err) {
            console.error(err, parts);
            return
        }
    }));
}


const main = async () => {
    const base = getBase('appHkDumX3ZZyboLO');
    console.log("new in tests last week");
    await updateTestsLastWeek(base, testsLastWeek.split('\n').filter((entry) => Boolean(entry && entry.trim())))
    console.log("monitored projects")
    await updateMonitored(base, monitoringProjects.split('\n').filter((entry) => Boolean(entry && entry.trim())), true);
    await updateMonitored(base, freeAccountsMonitoringProjects.split('\n').filter((entry) => Boolean(entry && entry.trim())), false);

}

main().then(() => { }).catch((err) => { console.error(err) });