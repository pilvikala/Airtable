export {};

declare global {
    interface Array<T> {
        deduplicate(): Array<T>;
    }
}

Array.prototype.deduplicate = function<T> () {
    const deduplicated: Array<T> = [];
    this.forEach((element)=>{
        if(!deduplicated.includes(element)) {
            deduplicated.push(element);
            return;
        }
    });
    return deduplicated;
}
